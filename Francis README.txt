Welcome to brick break.

Runs on mobile and pc browsers.
Tap/Click to play.
Break a row of Bricks to recieve two new balls.
Gameover happens when the player runs out of Balls or a Brick reaches the bottom.
Simply Tap/Click to reset the game.

~~

Known issues:
Game does not account for device orientation change mid game. Page has to be reloaded for correct scaling.
High score only tracks rows destroyed.
The ball rebounds a bit too fast off the left side of the paddle.
