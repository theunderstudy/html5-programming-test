
export class Ball extends Phaser.Sprite {

    constructor(game: Phaser.Game, xpos: number, ypos: number, key: string, xScale: number, yScale: number) {

        super(game, xpos, ypos, key)
        game.physics.enable(this);
        this.body.bounce.y = 1;
        this.body.bounce.x = 1;
        this.body.collideWorldBounds = true;
        this.scale.setTo(xScale, yScale);
        //this.LaunchBall(-350, -350);
        game.add.existing(this);
    }

    //speed increases
    VelocityMuliplyer: number = 1.05;
    private XClamp = -800;
    private YClamp = -800;

    public LaunchBall(xVelocity: number, yVelocity: number): void {
        //console.log("velocity set " + xVelocity + " " + yVelocity + " multi = " + this.VelocityMuliplyer);

        let desiredX = xVelocity * this.VelocityMuliplyer;
        let desiredY = yVelocity * this.VelocityMuliplyer;
        if (desiredY < this.YClamp)
            desiredY = this.YClamp;
        if (desiredX < this.XClamp)
            desiredX = this.XClamp;
        this.body.velocity.x = desiredX;
        this.body.velocity.y = desiredY;

    }

    public LockBall(): void {
        //hold ball onto paddle

    }

}