export class BallDeadZone extends Phaser.Sprite {
    constructor(game: Phaser.Game, xpos: number, ypos: number, key: string, xScale: number, yScale: number) {

        super(game, xpos, ypos, key)
        game.physics.enable(this);
        this.body.immovable = true;
        this.scale.setTo(xScale, yScale);
        //set pivot to center
        this.anchor.setTo(0.5, 0.5);
        game.add.existing(this);
    }
}