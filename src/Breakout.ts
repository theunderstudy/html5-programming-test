import { Paddle } from "./Paddle";
import { Ball } from "./Ball";
import { Row } from "./Row";
import { BallDeadZone } from "./BallDeadZone";
export class Breakout extends Phaser.Game {


	constructor() {
		//get the desired height 600 or less
		//get the desired width
		super(window.innerWidth, window.innerHeight, Phaser.AUTO, '', { preload: preload, create: create, update: update });

		function preload() {
			this.load.image('square', 'assets/square.png');
			this.load.image('ball', 'assets/ball.png');
			this.load.image('paddle', 'assets/paddle.png');
		}
		//states for the game
		enum Gamestates { Playing, Waiting, GameOver }
		var CurrentState = Gamestates.Waiting;


		//list of balls in game
		var Balls: Array<Ball> = new Array<Ball>();
		//rows of bricks in the game
		var Rows: Array<Row> = new Array<Row>(0);
		var HealthForRow = 0;
		//row spawning
		var TimeBetweenRows = 13.0;
		var CurrentTimeBetweenRows = 0;
		var PlayerPaddle: Paddle;
		var DeadZone;
		var HighScoreText;
		var Score: number = 0;
		//reward balls per row destroyed
		var RewardBalls = 2;

		function create() {
			//enable arcade physics
			this.physics.startSystem(Phaser.Physics.ARCADE);
			//create paddle
			PlayerPaddle = new Paddle(this, window.innerWidth / 2, (window.innerHeight - (window.innerHeight / 10)), 'paddle', 2.5, 1.5);
			//create a ball dead zone
			DeadZone = new BallDeadZone(this, window.innerWidth / 2, (window.innerHeight), 'paddle', 15, 1.5);

			this.input.onTap.add(onTap, this);
			HighScoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });
			RestartGame(this);
		}

		///handles game states
		function update() {
			switch (CurrentState) {
				case Gamestates.Playing:
					//check for collision between the balls and the bricks
					Balls.forEach(ball => {
						Rows.forEach(row => {
							if (row.CheckForCollisions(ball, this)) {
								//row has been destroyed
								for (var ballCount = 0; ballCount < RewardBalls; ballCount += 1) {
									//spawn a new ball for the player
									let ball = new Ball(this, window.innerWidth / 2, (window.innerHeight - (window.innerHeight / 6)), 'ball', 0.5, 0.5);
									Balls.push(ball);
									ball.LaunchBall(this.rnd.integerInRange(-350, 350), this.rnd.integerInRange(350, 550));
								}
								HighScoreText.text =  'Score: ' + Score;
								//schedule row for deletion
								row.DeleteThis = true;
							}
						});
					});
					//check for rows which need deleting
					for (var index = Rows.length - 1; index > -1; index -= 1) {
						if (Rows[index].DeleteThis) {
							//nothing else should reference this row once this ref is removed
							Rows.splice(index, 1);
						}
					}
					//check for collision between the balls and the player paddle
					this.physics.arcade.collide(PlayerPaddle, Balls, BallCollidePaddle);
					//update paddle position
					PlayerPaddle.UpdatePaddlePosition(this);
					//check for new row spawn
					CheckRowSpawn(this);
					//check for gameover
					CheckGameOver(this);
					break;
				case Gamestates.Waiting:
					break;
				case Gamestates.GameOver:
					break;
				default: break;

			}

		}
		///updates timer between new rows and spawns new row when appropriate
		function CheckRowSpawn(game: Phaser.Game) {
			//add time.deltatime
			CurrentTimeBetweenRows += game.time.physicsElapsed;
			//check if it is time for a new row
			if (CurrentTimeBetweenRows > TimeBetweenRows) {
				CurrentTimeBetweenRows = 0;
				//move all existing rows down to make space
				Rows.forEach(row => {
					row.RelocateRow(game, (200 * 0.4 + 10));
				});
				//spawn a new row
				let row = new Row(game, HealthForRow += 1);
				Rows.push(row);
			}
		}
		///handles player input during GameStates.Waiting and GameStates.GameOver
		function onTap(doubleTap) {
			switch (CurrentState) {

				case Gamestates.Waiting:
					if (doubleTap)
						StartGame();
					break;
				case Gamestates.GameOver:
					if (doubleTap)
						RestartGame(this);
					break;
				default: break;
			}
		}

		///checks for balls or bricks colliding with deadzone 
		function CheckGameOver(game: Phaser.Game) {
			//check ball dead zone
			game.physics.arcade.collide(DeadZone, Balls, BallCollideDeadZone);
			//check is any brick has collided with the deadzone
			Rows.forEach(row => {
				if (row.CheckForBricksHittingDeadZone(DeadZone, game)) {
					//game over 
					CurrentState = Gamestates.GameOver;
					//destroy all balls
					Balls.forEach(ball => {
						ball.destroy();
					});
					Balls = [];
				}
			});
		}
		///destroys a ball when it collides with the dead zone
		function BallCollideDeadZone(deadZone: BallDeadZone, ball: Ball) {
			//remove reference to ball
			Balls.splice(Balls.indexOf(ball), 1);
			//destroy ball
			ball.destroy();
			//check if there are remaining balls
			if (Balls.length == 0) {
				//gameover
				CurrentState = Gamestates.GameOver;
			}
		}
		///resets game systems
		function RestartGame(game: Phaser.Game) {
			//destroy all bricks on screen
			Rows.forEach(element => {
				element.DestroyRow();
			});
			//clear array
			Rows = [];
			//reset brick health
			HealthForRow = 0;
			//create row
			let row = new Row(game, HealthForRow += 1);
			Rows.push(row);
			//reset row timer
			CurrentTimeBetweenRows = 0;
			//create first ball
			let ball = new Ball(game, window.innerWidth / 2, (window.innerHeight - (window.innerHeight / 6)), 'ball', 0.5, 0.5);
			Balls.push(ball);
			CurrentState = Gamestates.Waiting;
		}

		function StartGame() {
			Balls[0].LaunchBall(0, -350);
			CurrentState = Gamestates.Playing;
		}

		//max x velocity on a rebound
		var ReboundXMax = 500;

		///rebounds the ball off of the paddle in a direction based on the angle from the center of the paddle
		function BallCollidePaddle(paddle: Paddle, ball: Ball) {
			//the x velocity should scale from the center of the paddle to the outside 
			//- so hitting the center would be x=0 and hitting the outside would x=max
			//get percentage
			let percentage = ball.x < (paddle.position.x)
				? /*ball is on the left*/InverseLerp((paddle.position.x), paddle.position.x - paddle.width / 2, ball.position.x)
				:/* ball is on the right*/InverseLerp((paddle.position.x), paddle.position.x + paddle.width / 2, ball.position.x);
			let xVelocity = ball.x < (paddle.position.x) ? Lerp(0, ReboundXMax, percentage) * -1 : Lerp(0, ReboundXMax, percentage);

			//for now, the y velocity should just invert
			let yVelocity = ball.body.velocity.y;
			ball.LaunchBall(xVelocity, yVelocity);
		}

		///Returns between min and max based on percentage
		function Lerp(min: number, max: number, percentage: number) {
			//console.log("min " + min + " max " + max + " percent " + percentage + " lerp " + (min + percentage * (max - min)));
			let value = min + percentage * (max - min);
			if (value < min)
				value = min;
			if (value > max)
				value = max;
			return value;
		}
		///finds the percentage value is between min and max
		function InverseLerp(min: number, max: number, value: number) {
			let percentge = ((value - min) / (max / min)) / 100;// over 100 to get 0-1
			if (percentge < 0)
				percentge *= -1;
			console.log("min " + min + " max " + max + " value " + value + " inverselerp " + percentge);

			return percentge
		}
	}
}