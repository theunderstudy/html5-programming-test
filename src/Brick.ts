import { Text } from "phaser";
import { Row } from "./Row";
export class Brick extends Phaser.Sprite {

    constructor(game: Phaser.Game, row: Row, xpos: number, ypos: number, key: string, xsize: number, ysize: number, startingHealth: number) {
        
        //create the brick
        super(game, xpos, ypos, key);
        game.physics.enable(this);
        this.body.immovable = true;
        this.scale.setTo(xsize, ysize);
        //assign ref to row
        this.BrickRow = row;
        //give health
        this.CurrentHealth = startingHealth;       
        //create health text
        this.HealthText = game.add.text(this.x + this.width / 2, this.y + this.height / 2, startingHealth.toString(), { fontSize: '32px', fill: '#14befc' });
        //anchor and position the text
        this.HealthText.anchor.setTo(0.5, 0.5);
        this.HealthText.position.x = Math.floor(this.x + this.width / 2);
        this.HealthText.position.y = Math.floor(this.y + this.height / 2);
        //swap the render order
        this.HealthText.renderOrderID = this.renderOrderID + 1;
        game.add.existing(this);
    }


    private CurrentHealth: number;
    public HealthText: Text;    
    private BrickRow: Row;
    ///removes itself from the row when dead
    public BallCollision(): void {
        this.CurrentHealth -= 1;
        if (this.CurrentHealth <= 0) 
        {
            this.HealthText.destroy();
            this.destroy(); 
            this.BrickRow.Bricks.splice(this.BrickRow.Bricks.indexOf(this),1 );
        }
        else {
            this.HealthText.text = this.CurrentHealth.toString();
        }
    }

   

    
}