import { Brick } from "./Brick";
import { Ball } from "./Ball";
import { BallDeadZone } from "./BallDeadZone";
export class Row {
    constructor(game: Phaser.Game, lifeForBricks: number) {
        this.CreateRowOfBricks(game, lifeForBricks);
    }

    public Bricks: Brick[];
    public DeleteThis: boolean = false;
    private BricksPerRow: number = 10;
    private BaseDistance: number = 10;
    ///creates a new row of bricks at the top of the screen
    private CreateRowOfBricks(game: Phaser.Game, lifeForBricks: number): void {
        //allocate array
        this.Bricks = new Array<Brick>(this.BricksPerRow);
        //create row of bricks
        let brick;
        ///calculate the spacing of the bricks
        //get size of brick sprite * scale multiplyer
        let sizeOfBrick = (200 * 0.4);
        let spaceBetweenBricks = (window.innerWidth - (sizeOfBrick * (this.BricksPerRow))) / this.BricksPerRow;
        for (var brickCount = 0; brickCount < this.BricksPerRow; brickCount += 1) {
            //create a row of bricks
            brick = new Brick(game, this, (spaceBetweenBricks + sizeOfBrick) * brickCount + this.BaseDistance, 10, 'square', 0.4, 0.4, lifeForBricks);
            this.Bricks[brickCount] = brick;
        }

    }

    ///checks if a ball has collided with any bricks in this row
    ///returns true if no bricks are left in the row
    public CheckForCollisions(ball: Ball, game: Phaser.Game): boolean {
        game.physics.arcade.collide(ball, this.Bricks, this.BallCollideBrick);
        //check if last brick in the row has been destroyed
        if (this.Bricks.length == 0) { return true; }

        return false;
    }
    ///checks if a brick has collided with the dead zone
    public CheckForBricksHittingDeadZone(deadZone: BallDeadZone, game: Phaser.Game): boolean {
        let collision = game.physics.arcade.collide(deadZone, this.Bricks);
        //if we have a collision
        if (collision)
            return true;
        return false;
    }
    //updates the brick with the new collision
    private BallCollideBrick(ball: Ball, brick: Brick): void {
        brick.BallCollision();
    }

    ///destroys all bricks in row left at the end of the game
    public DestroyRow() {
        for (var index = 0; index < this.Bricks.length; index += 1) {
            if (this.Bricks[index]) {
                this.Bricks[index].HealthText.destroy();
                this.Bricks[index].destroy();
            }
        }
    }
    ///moves row down to make space for new row
    public RelocateRow(game: Phaser.Game, distanceToMove: number) {
        for (var index = 0; index < this.Bricks.length; index += 1) {
            if (this.Bricks[index]) {


                game.add.tween(this.Bricks[index]).to({ y: this.Bricks[index].y + distanceToMove }, 1000
                    , Phaser.Easing.Quadratic.Out, true);

                game.add.tween(this.Bricks[index].HealthText).to({ y: this.Bricks[index].HealthText.y + distanceToMove }, 1000
                    , Phaser.Easing.Quadratic.Out, true);

            }
        }

    }
}